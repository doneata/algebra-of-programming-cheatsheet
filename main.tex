\documentclass[10pt,landscape]{article}

\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{hyperref}
\usepackage{tikz-cd}

% To make this come out properly in landscape mode, do one of the following
% 1.
%  pdflatex latexsheet.tex
%
% 2.
%  latex latexsheet.tex
%  dvips -P pdf  -t landscape latexsheet.dvi
%  ps2pdf latexsheet.ps

% If you're reading this, be prepared for confusion.  Making this was
% a learning experience for me, and it shows.  Much of the placement
% was hacked in; if you make it better, let me know...

% 2008-04
% Changed page margin code to use the geometry package. Also added code for
% conditional page margins, depending on paper size. Thanks to Uwe Ziegenhagen
% for the suggestions.

% 2006-08
% Made changes based on suggestions from Gene Cooperman. <gene at ccs.neu.edu>


% To do:
% \listoffigures \listoftables
% \setcounter{secnumdepth}{0}


% This sets page margins to .5 inch if using letter paper, and to 1cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
	{ \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
	{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	}

% Turn off header and footer
\pagestyle{empty}
 

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%x
                                {\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                {-1explus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%
                                {\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {1ex plus .2ex}%
                                {\normalfont\small\bfseries}}
\makeatother

% Don't print section numbers
\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}

\let\c\cdot
\newcommand{\fork}[2]{\left\langle #1, #2 \right\rangle}
\newcommand{\cofork}[2]{\left[#1, #2\right]}
\newcommand{\cata}[1]{\left(\!\left[#1\right]\!\right)}
\def\Ff{\mathsf{F}}
\def\Gf{\mathsf{G}}
\def\Hf{\mathsf{H}}
\def\Tf{\mathsf{T}}
% -----------------------------------------------------------------------

\begin{document}

\raggedright
\footnotesize
\begin{multicols}{3}


% multicol parameters
% These lengths are set only within the two main columns
% \setlength{\columnseprule}{0.25pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\begin{center}
  \Large{\textbf{Algebra of programming}} \\
\end{center}

\section{Categories}

Composition is associative and has identity arrows as units.

\begin{tabular}{@{}ll@{}}
  $f \c (g \c h) = (f \c g) \c g$ & associativity          \\
  $id \c f = f = f \c id$         & invariance to identity \\
\end{tabular}

\section{Functors}

Functors preserve identity and composition.

\begin{tabular}{@{}ll@{}}
  $\Ff (id) = id$                 & identity    \\
  $\Ff (f \c g) = \Ff f \c \Ff g$ & composition \\
\end{tabular}

Similarly, bifunctors preserve identity and composition:

\begin{tabular}{@{}ll@{}}
  $\Ff (id, id) = id$                               & identity    \\
  $\Ff (f \c h, g \c k) = \Ff (f, g) \c \Ff (h, k)$ & composition \\
\end{tabular}

\section{Natural transformations}

\begin{tikzcd}
  \Ff B \ar[d, "\Ff h"] & \Gf B \ar[l, "\phi"] \ar[d, "\Gf h"] \\
  \Ff A                 & \Gf A \ar[l, "\phi"] \\
\end{tikzcd}

\begin{tabular}{@{}ll@{}}
  $\Ff h \c \phi = \phi \c \Gf h$ & coherence condition \\
\end{tabular}

\subsection{Composition of natural transformations}

\begin{tabular}{@{}ll@{}}
  $(\phi \c \psi)_A = \phi_A \c \psi_A$ & vertical composition \\
\end{tabular}

\section{Constructing datatypes}

\subsection{Initial objects}

\subsection{Terminal objects}

\section{Products and coproducts}

\subsection{Products}

\begin{tikzcd}
    & C \ar[ld, "f" above] \ar[d, "\fork{f}{g}"] \ar[rd, "g"] & \\
  A & A \times B \ar[l, "outl"] \ar[r, "outr" below]          & B \\
\end{tikzcd}

\begin{tabular}{@{}ll@{}}
   $h = \fork{f}{g} \equiv outl \c h = f \land outr \c h = g$ & universal property \\
   $outl \c \fork{f}{g} = f \land outr \c \fork{f}{g} = g$    & cancellation \\
   $id = \fork{outl}{outr}$                                   & reflection \\
   $\fork{h}{k} \c m = \fork{h \c m}{k \c m}$                 & fusion \\
\end{tabular}

\subsubsection{The product functor}

\begin{tikzcd}
  B \ar[d, "f"] & B \times D \ar[l, "outl"] \ar[d, "f \times g"] \ar[r, "outr" below] & D \ar[d, "g"] \\
  A             & A \times C \ar[l, "outl"] \ar[r, "outr" below]                      & C \\
\end{tikzcd}

\begin{tabular}{@{}ll@{}}
   $f \times g = \fork{f \c outl}{g \c outr}$                       & definition \\
   $\left(f \times g\right) \c \fork{p}{q} = \fork{f \c p}{g \c q}$ & fusion \\
\end{tabular}

\subsection{Coproducts}

\begin{tikzcd}
                                     & C                     & \\
  A \ar[r, "inr" below] \ar[ru, "f"] & A + B \ar[u, "f + g"] & B \ar[l, "inl"] \ar[lu, "g" above]\\
\end{tikzcd}

\begin{tabular}{@{}ll@{}}
   $h = \cofork{f}{g} \equiv h \c inl = f \land h \c inr$    & universal property \\
   $\cofork{f}{g} \c inl = f \land \cofork{f}{g} \c inr = g$ & cancellation \\
   $id = \cofork{outl}{outr}$                                & reflection \\
   $m \c \cofork{h}{k} = \cofork{h \c m}{k \c m}$            & fusion \\
\end{tabular}

\subsubsection{The coproduct functor}

\begin{tikzcd}
  B \ar[d, "f"] \ar[r, "inl" below] & B + D \ar[d, "f + g"] & D \ar[l, "inr"] \ar[d, "g"] \\
  A             \ar[r, "inl" below] & A + C                 & C \ar[l, "inr"] \\
\end{tikzcd}

\begin{tabular}{@{}ll@{}}
   $f + g = \fork{inl \c f}{inr \c g}$                             & definition \\
   $\cofork{p}{q} \c \left(f + g\right) = \cofork{f \c p}{g \c q}$ & fusion \\
\end{tabular}

\section{Initial algebras}

\begin{tikzcd}
  T \ar[d, "\cata{f}"] & \Gf B \ar[l, "\alpha"] \ar[d, "\Ff \cata{f}"] \\
  A                    & \Gf A \ar[l, "f"] \\
\end{tikzcd}

\begin{tabular}{@{}ll@{}}
  $h = \cata{f} \equiv h \c \alpha = f \c \Ff h$            & universal property \\
  $id = \cata{\alpha}$                                      & reflection \\
  $h \c \cata{f} = \cata{g} \Leftarrow h \c f = g \c \Ff h$ & fusion \\
\end{tabular}

\section{Type functors}

Let $\Ff$ be a bifunctor with a collections of initial algebras $\alpha_A : \Tf A \leftarrow \Ff (A, \Tf A)$.
The construction T can be made into a functor by defining: $\Tf f = \cata{\alpha \c \Ff (f, id)}$.

A catamorphism composed with its type functor can be expressed as a single catamorphisms:

\begin{tabular}{@{}ll@{}}
  $\cata{h} \c Tg = \cata{h \c \Ff (g, id)}$ & fusion \\
\end{tabular}

\end{multicols}
\end{document}
