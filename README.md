Category theory cheat sheet for the "Algebra of Programming" book.

The cheat sheet uses [the following template](http://wch.github.io/latexsheet/).

# Related work

- Algebra of programming
- Calculating functional programs

# Examples

This section gives some examples on how to use the cheatsheet.

# Banana-split law

# TODO

- [ ] Add example usage of the cheat sheet.
- [ ] Mention related cheat sheets.
